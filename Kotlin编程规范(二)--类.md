# 类

#### 与Java的主要区别:

- 默认全是public的,所以写一个类需要考虑的是他的使用范围
- 不再有Java中一个文件只能定义一个public类的限制,*.kt文件不再需要与内部类名匹配
- 默认就是final的,继承必须显式声明(这是个好东西)

#### 具体使用:

- 主构造函数与次构造函数

  ```kotlin
  class Person(val name: String, val age: Int) { // 主构造函数
      var parent: Person? = null
  	//次构造函数必须调用主构造函数
      constructor(name: String, age: Int, parent: Person) : this(name, age) { 
          // do something for parent
          this.parent = parent
      }
  }
  ```

  ​最开始我看到这个是很兴奋的,主构造函数必须被调用,所以Java中习惯的初始化方法可以直接放在主构造函数中,就不用写个莫名其妙的init函数了,但是在Android中,需要大量继承的都是基于Java的(View, Activity),依然得有init之类的初始化,甩不掉Java,只能在纯Kotlin代码中这样做

- open 

  ```kotlin
  open class Person {
      open fun whatever(): Int {
          return hashCode()
      }
  }

  class SuperMan : Person() {
      open override fun whatever(): Int {
          return super.whatever()
      }
  }
  ```

  不加open关键字,不能继承,不管是类,还是方法,或者属性,这样可以提升抽象的成本,仪式感够不够?所以以后写基类写得太烂就等着被批吧~~

- 继承属性

  这条我认为很重要,值得专门写一条,在项目中,我们写了很多类似这样的代码:

  ```java
  public abstract class MakeBlockDevice extends Device {
    public abstract DeviceBoardName getBoardName();
    public abstract String getDeviceName();
  }
  ```

  Kotlin终于可以告别这种东西了:

  ```kotlin
  abstract class MakeBlockDevice {
      abstract val deviceName: String
      abstract val deviceBoardName: String
  }

  class Mbot : MakeBlockDevice() {
      override val deviceName: String = "mbot"
      override val deviceBoardName: String = "mcore"
  }

  ```

  在(一)中就有提到,变量是可以重写get()方法的,这也是Kotlin和Java一个大区别,属性更多的不应该理解成变量,而是理解为状态,是可以承载逻辑的

- 接口

  因为之前就提过的区别,本质上,可以认为Kotlin的变量定义都其实是在定义set()/get()方法,所以接口显然可以定义变量:

  ```kotlin
  interface MyInterface {
      val i: Int
  }

  class MyClass(override val i: Int) : MyInterface
  ```

- 覆盖冲突

  直接抄文档:

  ```kotlin
  interface A {
      fun foo() { print("A") }
      fun bar()
  }

  interface B {
      fun foo() { print("B") }
      fun bar() { print("bar") }
  }

  class C : A {
      override fun bar() { print("bar") }
  }

  class D : A, B {
      override fun foo() {
          super<A>.foo()
          super<B>.foo()
      }

      override fun bar() {
          super<B>.bar()
      }
  }
  ```

- 可见性

  与Java是基本一致,从语法层面本来不用关注,但是别忘了,默认全部都是public的,不该暴露的不要暴露,以前写了多少public,现在就应该写多少private:

  ```kotlin
  open class Outer {
      private val a = 1
      protected open val b = 2
      internal val c = 3
      val d = 4  // 默认 public
      
      protected class Nested {
          public val e: Int = 5
      }
  }

  class Subclass : Outer() {
      // a 不可见
      // b、c、d 可见
      // Nested 和 e 可见
  	override val b = 5   // “b”为 protected
  }
  ```

- 泛型

  泛型还是直接看[文档](https://www.kotlincn.net/docs/reference/generics.html),我还没有完全理解,以后补充

- 委托属性

  - Observable

    AOP!!!具体应用场景容我三思

  - map

    这也是一个好东西,怎么使用也需要先想想

  - 普通Delegate

    感觉没啥用,后面再看看

  - Lazy

    同样没想到具体的应用场景

  - 1.1新特性,不好意思没看懂,以后再说...

