# 基础语法  


### 须知
以下内容建立在Kotlin基本语法已经大致了解的基础上,一脸懵逼的先看[官方文档](https://www.kotlincn.net/docs/reference/basic-syntax.html),本文档只是列举我认为需要注意的地方,与文档是互补的关系

### 需要注意的语法

###### 变量定义:
下面这段话是基于Java的角度来考虑的,从Java的角度看,Kotlin的变量有以下特点:

- Kotlin没有Java那么严格的变量定义,Java类中有get(),set()的在Kotlin里都能以 object.fieldName 的方式访问和设置

- val 有get(),没有set(),不能显式修改,但不能认为不可修改,如以下代码,isEmpty会随着length的改变而改变:

  ```kotlin
  var length = 0
  val isEmpty
      get() = length == 0
  ```

- var 有get(),有set(),可修改,在Android Studio中会以下划线标记

###### 空与非空判断:
- Kotlin默认是不为空的,包括大部分未加注解的Java代码,将会识别为非空类型(如未声明变量类型,将出现代码提示),例如:

  ```kotlin
  val string: String = Java.getString()
  ```

  ``` kotlin
  public class Java {
      public static String getString() {
          return null;
      }
  }
  ```

  ​
  以上代码将会在初始化/赋值该变量时崩溃,虽然可以通过注解解决,但是一直加注解也不是个事

- ?/?:的使用,可能为空的变量,可以加?当做非空访问,若为空,将返回一个null,可以用?:来判断null的情况,如下:  

  ```kotlin
  var string: String? = "test"
  val length = string?.length ?: 0
  ```


- !!可以理解为确信不为空,关闭编译检查,不建议使用,如下

  ```kotlin
  val s: String? = "df"
  s!!.length
  ```

###### 类型转换

- ```kotlin
  val aInt: Int? = a as? Int
  ```

  若a不能转化为Int?, 将返回为null

###### 数据类:

- data关键字  

  ```kotlin
  data class User(val name: String, val age: Int)
  ```

- 自动生成下列函数:
    - equals()/hashCode() ；
    - toString() 格式是 "User(name=John, age=42)"；
    - componentN()  函数按声明顺序对应于所有属性；
    - copy() 函数

###### 强大的when:
- 首先不能把when理解为switch,应该理解为N个if else,会从上到下依次判断,直到找到true为止,所以这样的代码是鼓励的:  

  ```kotlin
  val score = 60
  when {
      score >= 90 -> {
          // 优秀?
      }
      score > 80 -> {
          // 不错?
      }
      else -> throw RuntimeException("就是这么任性")
  }
  ```


- 类型自动推断  

  ```kotlin
  var i: Any = 1
  when (i) {
      is Int -> Log.d("lyh", "'i' is int: $i")
      !is String -> Log.d("lyh", "'i' is not String")
      else -> {
          //可以直接使用String的属性
          Log.d("lyh", "'i' is String: $i, length: ${i.length}")
      }
  }
  ```

###### 赋值前可以进行一系列操作,比如初始化别的变量:
```kotlin
var i = 1
i = if (true) {
    Log.d("lyh", "i 被赋值为4")
    4
} else {
    2
}
i = when(i) {
    4 -> 1
    else -> {
        Log.d("lyh", "i 被赋值为4")
        4
    }
}
```

###### 区间:
```kotlin
val x: IntRange = 1..10
val y: IntProgression = 1..10 step 2
val z: IntProgression = 100 downTo 0 step 4
```
- 注意,这不是Java中的list,完全两个东西,不要以为写了个1..10,就以为是list了,并不是

- 其中IntRange是特殊的step为1的,有特殊的优化,比如判断:  

  ```kotlin
  if (x in 1..100) {}
  ```

  IntRange只会单纯的比较x是否大于等于1,小于等于100,而IntProgression会进行遍历

- 使用区间循环:

  ```kotlin
  for (i in 1..10)
  ```

###### 密封类

- 扩展的枚举:

  ```kotlin
  sealed class Expr
  data class Const(val number: Double) : Expr()
  data class Sum(val e1: Expr, val e2: Expr) : Expr()
  object NotANumber : Expr()
  ```

  比枚举类强在能真正分类,并保存对应属性了

###### 匿名类

- Kotlin的匿名类比Java好用,可以这样:

  ```kotlin
  val onClickListener = object : View.OnClickListener {
  	val info = "存点信息"
  	override fun onClick(v: View?) {
  	}
  }
  println("${onClickListener.info}")
  ```

  这个实质上是编译器帮我们定义了对应的类型,但是有一个情况是帮不了你的:

  ```kotlin
  fun publicFoo() = object {
  	val x: String = "x"
  }
  ```

  public方法,这种只能给你返回Any了,毕竟静态语言

###### 函数定义

- 默认参数可以有效减少重载时的方法数

  ```kotlin
  fun method(a: Int, b: Int = 0, c: Int = 0, d: Int = 0, e: Int = 0) {

  }

  val x = method(4)		//只有必要参数
  val y = method(a = 4, e = 3, c = 4)	//当有多个参数时,这样是可以不按顺序的
  ```

- 简单函数定义

  ```kotlin
  fun double(x: Int): Int = x * 2
  ```

- 参数长度可变

  ```kotlin
  fun <T> asList(vararg ts: T): List<T> {
      val result = ArrayList<T>()
      for (t in ts) // ts is an Array
          result.add(t)
      return result
  }
  ```

- ###### 操作符重载

  ```kotlin
  public inline operator fun <T> MutableCollection<in T>.plusAssign(elements: Array<T>) {
      this.addAll(elements)
  }
  ```

  操作符重载本质上是跟扩展函数一样的宏,不是很推荐自己重载操作符,这个具体情况再讨论吧

- 中缀表示法

  跟重载操作符其实类似:

  ```kotlin
  // 给 Int 定义扩展
  infix fun Int.shl(x: Int): Int {
  ……
  }

  // 用中缀表示法调用扩展函数

  1 shl 2

  // 等同于这样

  1.shl(2)
  ```

  ​

- 内联函数

  不是特别重要的优化,后续补充

- 局部函数

  即在一个函数中再定义一个函数,当一个私有函数,仅仅是被另一个函数调用时可以使用,增加可读性

  ```kotlin
  fun sort(list: Array<Int>) {
    fun swap(x: Int, y: Int) {
      val tmp = list[x]
      list[x] = list[y]
      list[y] = tmp
    }
    //假装在排序,需要改变两个对应下标的数字
    for (item in list) {
      if (true) {
        swap(1, 2)
      }
    }
  }
  ```

  ​




###### 尾递归

```kotlin
tailrec fun add(x: Double, y: Int): Double {
	if (y <= 0) return x
	return add(x + 1, y - 1)
}
```

注意尾递归的写法,不能有任何保存状态的东西,不然转化不了尾递归,比如把最后一句改为:

```kotlin
return x + add(x + 1, y - 1)
```

如果不想去深究尾递归到底怎么转换的,可以不用尾递归,也可以自己用大数据测试会不会爆栈,不允许出现加了尾递归标识,却不能转换为尾递归的代码出现



###### this

```kotlin
class A { // 隐式标签 @A
    inner class B { // 隐式标签 @B
        fun Int.foo() { // 隐式标签 @foo
          val a = this@A // A 的 this
          val b = this@B // B 的 this

          val c = this // foo() 的接收者，一个 Int
          val c1 = this@foo // foo() 的接收者，一个 Int

          val funLit = lambda@ fun String.() {
            val d = this // funLit 的接收者
          }


          val funLit2 = { s: String ->
                         // foo() 的接收者，因为它包含的 lambda 表达式
                         // 没有任何接收者
                         val d1 = this
                        }
        }
    }
}
```

###### 异常

- 使用大体和Java一致,区别在于try/catch是表达式,也就是说可以这样:

  ```kotlin
  val hasError = try {
    throw RuntimeException()
    false
  } catch (runtimeException: RuntimeException) {
    true
  }
  ```

- Kotlin不再有必须捕获的异常这种操作,可以少很多看着就麻烦的代码

- Nothing类型,也属于类型自动推断:

  ```kotlin
  fun fail(message: String): Nothing {
      throw IllegalArgumentException(message)
  }
  val s = person.name ?: fail("Name required")
  println(s)     // 在此已知“s”已初始化
  ```

  ​

###### 高阶函数和Lambda表达式

- 直接看[文档](https://kotlinlang.org/docs/reference/lambdas.html),太多了,写不过来

- Lambda表达式需要特别注意的一点:他是表达式,而不是函数,虽然看着很像函数,但是不是函数,比如说在表达式中return,return掉的是外面的函数

- 匿名函数,不是太常用的样子,后续补充

- 闭包,因为我们之前基本没用Java的Lambda表达式,所以不需要去比较了,知道可以这么做就行了:

  ``` kotlin
  var sum = 0
  ints.filter { it > 0 }.forEach {
      sum += it
  }
  print(sum)
  ```

###### 协程

- 现在还是实验性的,不加入项目
- 后续肯定会加,可以简化很多代码