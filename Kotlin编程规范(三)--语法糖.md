# 语法糖

### 扩展

###### 扩展函数

```kotlin
fun MutableList<Int>.swap(index1: Int, index2: Int) {
    val tmp = this[index1] // “this”对应该列表
    this[index1] = this[index2]
    this[index2] = tmp
}

val list = mutableListOf(1, 2, 3)
list.swap(0, 2) // “swap()”内部的“this”得到“l”的值
```

###### 扩展属性

```kotlin
val <T> List<T>.lastIndex: Int
    get() = size - 1
val list = mutableListOf(1, 2, 3)
val i = list.lastIndex
```

###### 如何理解扩展是静态解析的

首先需要明确,Kotlin是一门静态语言,动态扩展显然不是静态语言可以真正支持的东西,扩展本质上跟C里面的宏是一个概念,说到底只是一系列文本替换而已,比如上文的lastIndex,可以理解为每次调用这个属性的地方,编译时自动把代码换成了这样:

```kotlin
val i = list.size - 1
```

所以这样的代码

```kotlin
open class C

class D: C()

fun C.foo() = "c"

fun D.foo() = "d"

fun printFoo(c: C) {
    println(c.foo())
}

printFoo(D())
```

会变成:

```kotlin
fun printFoo(c: C) {
    println("c")
}

printFoo(D())
```

扩展只是为我们不方便/不应该修改,偏偏又使用频繁的代码提供优雅的实现方式,使用需要慎重,不要滥用

### 解构声明

- 根据类的属性定义变量:

  ```kotlin
  data class Person(val name: String, val age: Int)
  val person = Person("zhangsan", 20)
  val (age, name) = person
  ```

  注意!!!

  这是特意写的错误的示范,这样的话,age是"zhangsan", name是20,变量是严格按照定义顺序的,不需要的参数用下划线忽略,这种用法只限于认为数据类足够稳定,不会修改的情况,否则不要使用

- 从函数中返回多个变量:

  ```kotlin
  data class Result(val result: Int, val status: Status)
  fun function(……): Result {
      // 各种计算
      return Result(result, status)
  }
  // 现在，使用该函数：
  val (result, status) = function(……)
  ```

- 访问一个map:

  ```kotlin
  for ((key, value) in map) {
     // 使用该 key、value 做些事情
  }
  ```

### 语言提供的各种简便方法

- ```kotlin
  mutableListOf(1, 2, 3)
  ```

- ```kotlin
  listOf(1, 2, 3)
  ```

- ```kotlin
  hashSetOf("a", "b", "c", "c")
  ```

- ```kotlin
  list.firstOrNull()
  ```

- 等等等等,还有很多自己去看


- ```kotlin
  // 生成一个全是-1的数组
  fun arrayOfMinusOnes(size: Int): IntArray {
      return IntArray(size).apply { fill(-1) }
  }
  ```

- ```kotlin
  // 生成一个x平方的List
  val list = List(10) {
    it * it
  }
  ```

- ```kotlin
  // 生成新的List,选出大于10的元素
  val list = testList.filter { it > 10 }
  ```

- 类似的很多也自己去看

### 还没看那么细的,但是后续肯定会用的

- [类型安全的构造器](https://www.kotlincn.net/docs/reference/type-safe-builders.html),貌似算DSL?特别适合协议层的编写,可以真正做到代码即文档:

  ```kotlin
  fun result(args: Array<String>) =
      html {
          head {
              title {+"XML encoding with Kotlin"}
          }
          body {
              h1 {+"XML encoding with Kotlin"}
              p  {+"this format can be used as an alternative markup to XML"}

              // 一个具有属性和文本内容的元素
              a(href = "http://kotlinlang.org") {+"Kotlin"}

              // 混合的内容
              p {
                  +"This is some"
                  b {+"mixed"}
                  +"text. For more see the"
                  a(href = "http://kotlinlang.org") {+"Kotlin"}
                  +"project"
              }
              p {+"some text"}

              // 以下代码生成的内容
              p {
                  for (arg in args)
                      +arg
              }
          }
      }
  ```

  当然,看上去美好的代码,是需要下面这样的代码支持的:

  ```kotlin
  interface Element {
      fun render(builder: StringBuilder, indent: String)
  }

  class TextElement(val text: String) : Element {
      override fun render(builder: StringBuilder, indent: String) {
          builder.append("$indent$text\n")
      }
  }

  @DslMarker
  annotation class HtmlTagMarker

  @HtmlTagMarker
  abstract class Tag(val name: String) : Element {
      val children = arrayListOf<Element>()
      val attributes = hashMapOf<String, String>()

      protected fun <T : Element> initTag(tag: T, init: T.() -> Unit): T {
          tag.init()
          children.add(tag)
          return tag
      }

      override fun render(builder: StringBuilder, indent: String) {
          builder.append("$indent<$name${renderAttributes()}>\n")
          for (c in children) {
              c.render(builder, indent + "  ")
          }
          builder.append("$indent</$name>\n")
      }

      private fun renderAttributes(): String {
          val builder = StringBuilder()
          for ((attr, value) in attributes) {
              builder.append(" $attr=\"$value\"")
          }
          return builder.toString()
      }

      override fun toString(): String {
          val builder = StringBuilder()
          render(builder, "")
          return builder.toString()
      }
  }

  abstract class TagWithText(name: String) : Tag(name) {
      operator fun String.unaryPlus() {
          children.add(TextElement(this))
      }
  }

  class HTML : TagWithText("html") {
      fun head(init: Head.() -> Unit) = initTag(Head(), init)

      fun body(init: Body.() -> Unit) = initTag(Body(), init)
  }

  class Head : TagWithText("head") {
      fun title(init: Title.() -> Unit) = initTag(Title(), init)
  }

  class Title : TagWithText("title")

  abstract class BodyTag(name: String) : TagWithText(name) {
      fun b(init: B.() -> Unit) = initTag(B(), init)
      fun p(init: P.() -> Unit) = initTag(P(), init)
      fun h1(init: H1.() -> Unit) = initTag(H1(), init)
      fun a(href: String, init: A.() -> Unit) {
          val a = initTag(A(), init)
          a.href = href
      }
  }

  class Body : BodyTag("body")
  class B : BodyTag("b")
  class P : BodyTag("p")
  class H1 : BodyTag("h1")

  class A : BodyTag("a") {
      var href: String
          get() = attributes["href"]!!
          set(value) {
              attributes["href"] = value
          }
  }

  fun html(init: HTML.() -> Unit): HTML {
      val html = HTML()
      html.init()
      return html
  }
  ```

  ​



