# 设计模式

### 须知

这部分内容显然是在摸索阶段,每个人在新语言的特性,以及对每个设计模式的理解都不一定真正到位,随时修改

# 实现

###### 单例:

```kotlin
object DataProviderManager {
    fun registerDataProvider(provider: DataProvider) {
        // ……
    }

    val allDataProviders: Collection<DataProvider>
        get() = // ……
}
```

```kotlin
DataProviderManager.registerDataProvider(……)
```

###### 工厂:

```kotlin
interface Factory<T> {
    fun create(): T
}

class MyClass {
    companion object : Factory<MyClass> {
        override fun create(): MyClass = MyClass()
    }
}
```



###### 委托:

```kotlin
interface Base {
    fun print()
}

class BaseImpl(val x: Int) : Base {
    override fun print() { print(x) }
}

class Derived(b: Base) : Base by b

fun main(args: Array<String>) {
    val b = BaseImpl(10)
    Derived(b).print() // 输出 10
}
```

注:委托模式之前了解比较少,现在看起来,很适合用于我们通讯(经典蓝牙,BLE,WIFI等)的需求





