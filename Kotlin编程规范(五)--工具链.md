# 工具链

### 通过kapt支持注解处理

- [文档](https://www.kotlincn.net/docs/reference/kapt.html)
- databinding, dagger等都需要这样支持

### 文档生成

- [dokka](https://github.com/Kotlin/dokka)

