# 项目简介

推荐使用[Typora](https://typora.io/)(需翻墙)编辑器查看修改

本项目用于介绍:

- Kotlin区别于Java的语法
- 一些常用语法糖
- 常用设计模式的实现
- Android相关的一些库在Kotlin环境下的使用区别

